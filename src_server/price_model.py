"""
 Copyright (C) Dávid Kubík - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
 Written by Dávid Kubík <kubik.david5@gmail.com>, April 2018
"""


"constants:"
BATTERY_PRICE = 425
BATTERY_LIFETIME = 10  # in years
SYSTEM_LIFETIME = 20


def get_battery_replacements(battery_num):
    if battery_num > 0:
        return (SYSTEM_LIFETIME / BATTERY_LIFETIME)
    else:
        return 1


def get_battery_costs(battery_num):
    return battery_num * BATTERY_PRICE * (SYSTEM_LIFETIME / BATTERY_LIFETIME)


def get_pv_modules_costs(pv_power, pv_price, pv_num):  # pv_power in kW
    if (pv_power / pv_num) < 0.2:
        return pv_num * 0.2 * pv_price * 1000  # if optimization fails (recommended PV module produces too little power)
    else:
        return pv_power * pv_price * 1000


def get_inverter_costs(pv_power):  # pv_power in kW
    if pv_power <= 1.500:
        return 520
    elif 1.500 < pv_power <= 2.000:
        return 590
    elif 2.000 < pv_power <= 2.500:
        return 650
    elif 2.500 < pv_power <= 4.000:
        return 1090
    elif 4.000 < pv_power <= 10.000:
        return 2560
    elif 10.000 < pv_power:
        return 999999  # not defined, but it has to return float


def get_controller_costs(pv_power, battery_num):  # pv_power in kW
    if battery_num > 0:
        if pv_power <= 4.500:
            return 219
        elif 4.500 < pv_power <= 7.500:
            return 259
        else:
            return 999999  # not defined, but it has to return float
    else:
        return 0


def get_installation_costs(pitched_roof, pv_num, pv_power, battery_num):  # pv_power in kW
    dict = {}

    if pitched_roof:
        dict["pv"] = pv_num * 53
    else:
        dict["pv"] = pv_num * 86

    if (pv_power / pv_num) < 0.2:
        dict["system"] = pv_num * 0.2 * 0.29 * 1000
    else:
        dict["system"] = pv_power * 0.29 * 1000

    dict["battery"] = battery_num * 15 * (SYSTEM_LIFETIME / BATTERY_LIFETIME)

    if battery_num > 0:
        dict["controller"] = 10
    else:
        dict["controller"] = 0

    return dict
