"""
 Copyright (C) Dávid Kubík - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
 Written by Dávid Kubík <kubik.david5@gmail.com>, April 2018
"""

from pv_module_optimizer import compute

from flask import Flask, jsonify, request
from datetime import datetime

app = Flask(__name__)


@app.route('/')
def index():
    return 'Server is running...'


@app.route('/compute', methods=['POST'])
def post_json():
    print(datetime.now(), "Request for POST JSON and compute from IP address:", request.remote_addr)

    if (request.is_json):
        content = request.get_json()

        latitude = content[0]['latitude']
        longitude = content[0]['longitude']
        altitude = content[0]['altitude']
        pv_num = content[0]['pvNum']
        battery_num = content[0]['batteryNum']
        azimuth = content[0]['azimuth']
        pitched_roof = content[0]['pitchedRoof']
        el_price = content[0]['elPrice']
        pv_price = content[0]['pvPrice']
        use_weekend = content[0]['useWeekend']

        workday = []
        workday.append(content[1]['workday'][0]['zero'])
        workday.append(content[1]['workday'][1]['one'])
        workday.append(content[1]['workday'][2]['two'])
        workday.append(content[1]['workday'][3]['three'])
        workday.append(content[1]['workday'][4]['four'])
        workday.append(content[1]['workday'][5]['five'])
        workday.append(content[1]['workday'][6]['six'])
        workday.append(content[1]['workday'][7]['seven'])
        workday.append(content[1]['workday'][8]['eight'])
        workday.append(content[1]['workday'][9]['nine'])
        workday.append(content[1]['workday'][10]['ten'])
        workday.append(content[1]['workday'][11]['eleven'])
        workday.append(content[1]['workday'][12]['twelve'])
        workday.append(content[1]['workday'][13]['thirteen'])
        workday.append(content[1]['workday'][14]['fourteen'])
        workday.append(content[1]['workday'][15]['fifteen'])
        workday.append(content[1]['workday'][16]['sixteen'])
        workday.append(content[1]['workday'][17]['seventeen'])
        workday.append(content[1]['workday'][18]['eighteen'])
        workday.append(content[1]['workday'][19]['nineteen'])
        workday.append(content[1]['workday'][20]['twenty'])
        workday.append(content[1]['workday'][21]['twentyone'])
        workday.append(content[1]['workday'][22]['twentytwo'])
        workday.append(content[1]['workday'][23]['twentythree'])

        weekend = []
        weekend.append(content[2]['weekend'][0]['zero'])
        weekend.append(content[2]['weekend'][1]['one'])
        weekend.append(content[2]['weekend'][2]['two'])
        weekend.append(content[2]['weekend'][3]['three'])
        weekend.append(content[2]['weekend'][4]['four'])
        weekend.append(content[2]['weekend'][5]['five'])
        weekend.append(content[2]['weekend'][6]['six'])
        weekend.append(content[2]['weekend'][7]['seven'])
        weekend.append(content[2]['weekend'][8]['eight'])
        weekend.append(content[2]['weekend'][9]['nine'])
        weekend.append(content[2]['weekend'][10]['ten'])
        weekend.append(content[2]['weekend'][11]['eleven'])
        weekend.append(content[2]['weekend'][12]['twelve'])
        weekend.append(content[2]['weekend'][13]['thirteen'])
        weekend.append(content[2]['weekend'][14]['fourteen'])
        weekend.append(content[2]['weekend'][15]['fifteen'])
        weekend.append(content[2]['weekend'][16]['sixteen'])
        weekend.append(content[2]['weekend'][17]['seventeen'])
        weekend.append(content[2]['weekend'][18]['eighteen'])
        weekend.append(content[2]['weekend'][19]['nineteen'])
        weekend.append(content[2]['weekend'][20]['twenty'])
        weekend.append(content[2]['weekend'][21]['twentyone'])
        weekend.append(content[2]['weekend'][22]['twentytwo'])
        weekend.append(content[2]['weekend'][23]['twentythree'])

        return jsonify(compute(latitude, longitude, altitude, pv_num, battery_num, azimuth, pitched_roof,
                               el_price, pv_price, use_weekend, workday, weekend))
    else:
        return None


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000, threaded=True)
