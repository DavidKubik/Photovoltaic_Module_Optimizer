"""
 Copyright (C) Dávid Kubík - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
 Written by Dávid Kubík <kubik.david5@gmail.com>, October 2017
"""

import numpy as np
"import needed for reading csv file with load demand of electricity data:"
from numpy import genfromtxt

def scale(x, min, max):
    if min == max:
        return 1.0
    else:
        return (x - min) / (max - min)

def find_best_module(found_param, modules_path, mins, maxs):
    temp_sandia_modules = genfromtxt(modules_path, delimiter=',', dtype=str)  # dtype is string so that names are also read
    temp_sandia_modules = temp_sandia_modules[3:]  # cut the headers
    sandia_modules = []
    index = 0

    for i in range(len(temp_sandia_modules)):
        module_param = []

        module_param.append(temp_sandia_modules[i, 12])  # C0
        module_param.append(temp_sandia_modules[i, 13])  # C1
        module_param.append(temp_sandia_modules[i, 19])  # C2
        module_param.append(temp_sandia_modules[i, 20])  # C3
        module_param.append(temp_sandia_modules[i, 18])  # N
        module_param.append(temp_sandia_modules[i, 8])  # Impo
        module_param.append(temp_sandia_modules[i, 11])  # Aimp
        module_param.append(temp_sandia_modules[i, 9])  # Vmpo
        module_param.append(temp_sandia_modules[i, 16])  # Bvmpo
        module_param.append(temp_sandia_modules[i, 4])  # Cells in series
        module_param.append(temp_sandia_modules[i, 21])  # A0
        module_param.append(temp_sandia_modules[i, 22])  # A1
        module_param.append(temp_sandia_modules[i, 23])  # A2
        module_param.append(temp_sandia_modules[i, 24])  # A3
        module_param.append(temp_sandia_modules[i, 25])  # A4
        module_param.append(temp_sandia_modules[i, 26])  # B0
        module_param.append(temp_sandia_modules[i, 27])  # B1
        module_param.append(temp_sandia_modules[i, 28])  # B2
        module_param.append(temp_sandia_modules[i, 29])  # B3
        module_param.append(temp_sandia_modules[i, 30])  # B4
        module_param.append(temp_sandia_modules[i, 31])  # B5
        module_param.append(temp_sandia_modules[i, 33])  # FD
        module_param.append(temp_sandia_modules[i, 0])  # Name
        module_param.append(temp_sandia_modules[i, 1])  # Vintage
        module_param.append(temp_sandia_modules[i, 3])  # Material
        module_param.append(temp_sandia_modules[i, 2])  # Area

        "find fitness of pv modules:"
        module_param.append(find_fitness(found_param, module_param, mins, maxs))

        temp_sandia_module = np.asarray(module_param)  # convert list to array
        sandia_modules.append(temp_sandia_module)  # add array to list of arrays


        "find best match:"
        if i == 0:
            best_fitness = float(temp_sandia_module[26])
        elif float(best_fitness) > float(temp_sandia_module[26]):
            best_fitness = float(temp_sandia_module[26])
            index = i

    sandia_modules = np.stack(sandia_modules, 0)  # convert list of arrays to 2d array

    return sandia_modules[index]


def find_fitness(found_param, module_param, mins, maxs):
    # find fitness according to normalized Euclidean distance with weights
    result = []
    fitness = 0.0

    # it is needed to clamp values to <0;1> scale before subtracting them (because of the negative numbers)
    result.append(pow(scale(found_param[0], mins[0], maxs[0]) - scale(float(module_param[0]), mins[0], maxs[0]), 2))  # C0
    result.append(pow(scale(found_param[1], mins[1], maxs[1]) - scale(float(module_param[1]), mins[1], maxs[1]), 2))  # C1
    result.append(pow(scale(found_param[2], mins[2], maxs[2]) - scale(float(module_param[2]), mins[2], maxs[2]), 2))  # C2
    result.append(pow(scale(found_param[3], mins[3], maxs[3]) - scale(float(module_param[3]), mins[3], maxs[3]), 2))  # C3
    result.append(pow(scale(found_param[4], mins[4], maxs[4]) - scale(float(module_param[4]), mins[4], maxs[4]), 2))  # N
    result.append(pow(scale(found_param[5], mins[5], maxs[5]) - scale(float(module_param[5]), mins[5], maxs[5]), 2))  # Impo
    result.append(pow(scale(found_param[6], mins[6], maxs[6]) - scale(float(module_param[6]), mins[6], maxs[6]), 2))  # Aimp
    result.append(pow(scale(found_param[7], mins[7], maxs[7]) - scale(float(module_param[7]), mins[7], maxs[7]), 2))  # Vmpo
    result.append(pow(scale(found_param[8], mins[8], maxs[8]) - scale(float(module_param[8]), mins[8], maxs[8]), 2))  # Bvmpo
    result.append(pow(scale(found_param[9], mins[9], maxs[9]) - scale(float(module_param[9]), mins[9], maxs[9]), 2))  # Cells in series
    result.append(pow(scale(found_param[10], mins[10], maxs[10]) - scale(float(module_param[10]), mins[10], maxs[10]), 2))  # A0
    result.append(pow(scale(found_param[11], mins[11], maxs[11]) - scale(float(module_param[11]), mins[11], maxs[11]), 2))  # A1
    result.append(pow(scale(found_param[12], mins[12], maxs[12]) - scale(float(module_param[12]), mins[12], maxs[12]), 2))  # A2
    result.append(pow(scale(found_param[13], mins[13], maxs[13]) - scale(float(module_param[13]), mins[13], maxs[13]), 2))  # A3
    result.append(pow(scale(found_param[14], mins[14], maxs[14]) - scale(float(module_param[14]), mins[14], maxs[14]), 2))  # A4
    result.append(pow(scale(found_param[15], mins[15], maxs[15]) - scale(float(module_param[15]), mins[15], maxs[15]), 2))  # B0
    result.append(pow(scale(found_param[16], mins[16], maxs[16]) - scale(float(module_param[16]), mins[16], maxs[16]), 2))  # B1
    result.append(pow(scale(found_param[17], mins[17], maxs[17]) - scale(float(module_param[17]), mins[17], maxs[17]), 2))  # B2
    result.append(pow(scale(found_param[18], mins[18], maxs[18]) - scale(float(module_param[18]), mins[18], maxs[18]), 2))  # B3
    result.append(pow(scale(found_param[19], mins[19], maxs[19]) - scale(float(module_param[19]), mins[19], maxs[19]), 2))  # B4
    result.append(pow(scale(found_param[20], mins[20], maxs[20]) - scale(float(module_param[20]), mins[20], maxs[20]), 2))  # B5
    result.append(pow(scale(found_param[21], mins[21], maxs[21]) - scale(float(module_param[21]), mins[21], maxs[21]), 2))  # FD

    for i in range(len(result)):
        fitness += result[i]

    return fitness ** 0.5
