from pv_module_optimizer import compute

import pandas as pd
import time

start = time.time()

SOURCE_NAME = "../data/austin_households.csv"
TEMP_SOURCE = "../data/temp_austin_households.csv"
FILE_RESULT_NAME = "../results/result_austin_households.csv"

pv_num = 9
battery_num = 2
azimuth = 180
pitched_roof = True
el_price = 0.1016  # https://www.bounceenergy.com/ DOLLAR = 0.8470 EUR
pv_price = 0.65
sum = 0

with open(FILE_RESULT_NAME, "w") as file:
    file.write("household_num,pv_name,timezone,annual_generated,annual_consumption,cov_system,cov_pv,cov_battery,"
               "annual_savings,max_power,max_consumption,battery_rep_num,battery_costs,pv_costs,inverter_costs,"
               "controller_costs,pv_install,system_install,battery_install,controller_install,total_savings,"
               "total_costs,net_savings,pay_back\n")
file.close()

# Austin, Texas, USA
latitude = 30.268076
longitude = -97.742220
altitude = 162

df = pd.read_csv(SOURCE_NAME)
df = df['use']

file_count = 0
for i in range(115):
    start_house = time.time()
    df2 = df[file_count * 8760: file_count * 8760 + 8760]
    df2.to_csv(TEMP_SOURCE, index=False)

    dict = compute(latitude, longitude, altitude, pv_num, battery_num, azimuth, pitched_roof,
            el_price, pv_price, use_data_from_file=True, file_path=TEMP_SOURCE)

    with open(FILE_RESULT_NAME, "a") as file:
        file.write("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},"
                   "{21},{22},{23}\n".format(i,
                                             dict["pv_name"],
                                             dict["timezone"],
                                             dict["annual_generated"],
                                             dict["annual_consumption"],
                                             dict["cov_system"],
                                             dict["cov_pv"],
                                             dict["cov_battery"],
                                             dict["annual_savings"],
                                             dict["max_power"],
                                             dict["max_consumption"],
                                             dict["battery_rep_num"],
                                             dict["battery_costs"],
                                             dict["pv_costs"],
                                             dict["inverter_costs"],
                                             dict["controller_costs"],
                                             dict["pv_install"],
                                             dict["system_install"],
                                             dict["battery_install"],
                                             dict["controller_install"],
                                             dict["total_savings"],
                                             dict["total_costs"],
                                             dict["net_savings"],
                                             dict["pay_back"]))
    file.close()

    file_count += 1

    end_house = time.time()
    sum += end_house - start_house
    house_time = sum / file_count

    print("PROGRESS: {0}/{1} ~ {2}%    ESTIMATED REMAINING TIME: {3} hour {4} min {5} sec\n\n".format(
        file_count,
        115,
        round((file_count / 115) * 100, 2),
        int(((115 - file_count) * house_time) / 3600),
        int((((115 - file_count) * house_time) % 3600) / 60),
        round(((115 - file_count) * house_time) % 60)))

end = time.time()
print("\nTesting household locations finished in:", int((end - start) / 60), "min", round((end - start) % 60, 1), "sec")
