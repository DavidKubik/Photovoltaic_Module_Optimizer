from pv_module_optimizer import compute

"computation time measure:"
import time

start = time.time()

FILE_RESULT_NAME = "../results/result_dallas_locations.csv"
DATA_SOURCE = "../data/average_household_usage_dallas.csv"

ireland_dublin = [53.359151, -6.280032, 40]  # latitude, longitude, altitude
usa_austin = [30.268076, -97.742220, 162]
usa_new_york = [40.718392, -73.983958, 16]
slovakia_bratislava = [48.153730, 17.072004, 186]
egypt_kahira = [30.029898, 31.260861, 95]
germany_berlin = [52.518535, 13.376498, 62]
china_peking = [39.911568, 116.399615, 73]
south_africa_cape_town = [-33.916670, 18.428221, 22]
canada_ottawa = [45.422993, -75.693614, 96]
locations = [ireland_dublin, usa_austin, usa_new_york, slovakia_bratislava, egypt_kahira, germany_berlin, china_peking,
             south_africa_cape_town, canada_ottawa]

pv_num = 9
battery_num = 2
azimuth = 180
pitched_roof = True
el_price = 0.1566616
pv_price = 0.65

with open(FILE_RESULT_NAME, "w") as file:
    file.write("location_num,pv_name,timezone,annual_generated,annual_consumption,cov_system,cov_pv,cov_battery,"
               "annual_savings,max_power,max_consumption,battery_rep_num,battery_costs,pv_costs,inverter_costs,"
               "controller_costs,pv_install,system_install,battery_install,controller_install,total_savings,"
               "total_costs,net_savings,pay_back\n")
file.close()

sum = 0
locations_num = len(locations)

for i in range(locations_num):
    start_house = time.time()

    latitude = locations[i][0]
    longitude = locations[i][1]
    altitude = locations[i][2]
    dict = compute(latitude, longitude, altitude, pv_num, battery_num, azimuth, pitched_roof, el_price, pv_price,
                   use_data_from_file=True, file_path=DATA_SOURCE)

    with open(FILE_RESULT_NAME, "a") as file:
        file.write("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},"
                   "{21},{22},{23}\n".format(i+1,
                                             dict["pv_name"],
                                             dict["timezone"],
                                             dict["annual_generated"],
                                             dict["annual_consumption"],
                                             dict["cov_system"],
                                             dict["cov_pv"],
                                             dict["cov_battery"],
                                             dict["annual_savings"],
                                             dict["max_power"],
                                             dict["max_consumption"],
                                             dict["battery_rep_num"],
                                             dict["battery_costs"],
                                             dict["pv_costs"],
                                             dict["inverter_costs"],
                                             dict["controller_costs"],
                                             dict["pv_install"],
                                             dict["system_install"],
                                             dict["battery_install"],
                                             dict["controller_install"],
                                             dict["total_savings"],
                                             dict["total_costs"],
                                             dict["net_savings"],
                                             dict["pay_back"]))
    file.close()

    end_house = time.time()
    sum += end_house - start_house
    house_time = sum / (i+1)

    print("PROGRESS: {0}/{1} ~ {2}%    ESTIMATED REMAINING TIME: {3} hour {4} min {5} sec\n\n".format(
        i+1,
        locations_num,
        round(((i+1) / locations_num) * 100, 2),
        int(((locations_num - (i+1)) * house_time) / 3600),
        int((((locations_num - (i+1)) * house_time) % 3600) / 60),
        round(((locations_num - (i+1)) * house_time) % 60)))

end = time.time()
print("\nTesting household locations finished in:", int((end - start) / 60), "min", round((end - start) % 60, 1), "sec")
