"""
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

Neither the name of the {organization} nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.
"""

from module_fitness import find_best_module
from battery_model import discharge_battery
from battery_model import rest_battery_cap_kwh
from battery_model import charge_battery
from price_model import get_battery_replacements
from price_model import get_battery_costs
from price_model import get_pv_modules_costs
from price_model import get_inverter_costs
from price_model import get_controller_costs
from price_model import get_installation_costs

import numpy as np
import timezonefinder

"computation time measure:"
import time

"imports needed for effective irradiance:"
import pandas as pd
import datetime
import pvlib
from pvlib.location import Location

"import needed for reading csv file with load demand of electricity data:"
from numpy import genfromtxt

"function that is called from server:"
def compute(latitude, longitude, altitude, pv_num, battery_num, azimuth, pitched_roof, el_price, pv_price,
           use_weekend=True, workday=None, weekend=None, use_data_from_file=False, file_path=None):
    "constants:"
    DAYS_NO = 364
    MODULES_PATH = "../data/sam-library-sandia-modules-2015-6-30.csv"
    SANDIA_MODULES = pvlib.pvsystem.retrieve_sam(name='SandiaMod')
    T0 = 25
    q = 1.60218e-19  # elementary charge in units of coulombs
    kb = 1.38066e-23  # Boltzmann's constant in units of J/K
    # WIND_SPEED and TEMP_CELL changes savings per year only approximately up to 0.03%
    WIND_SPEED = 9
    TEMP_CELL = 30  # cell temperature in particular hours
    MEASUREMENTS_PER_DAY = 24
    BATTERY_CAP = 2.52  # battery capacity of a single battery unit (in kWh)
    SYSTEM_LIFETIME = 20.06868  # because we want to get 7305 days from 364 days (including leap years)
    # get PV modules database length:
    temp_sandia_modules = genfromtxt(MODULES_PATH, delimiter=',',
                                     dtype=str)  # dtype is string so that names of PV modules are also read
    temp_sandia_modules = temp_sandia_modules[3:]  # cut the headers
    DB_LEN = len(temp_sandia_modules)

    if use_data_from_file:
        household_data = genfromtxt(file_path, delimiter=',',dtype=float)
    else:
        "generate electricity consumption data for 364 days according to typical week defined by user:"
        household_data = np.array([])
        if use_weekend:
            household_data = np.tile(workday, 5)
            household_data = np.append(household_data, np.tile(weekend, 2))
            household_data = np.tile(household_data, 52)
        else:
            household_data = np.tile(workday, 364)

    bat_kwh = battery_num * BATTERY_CAP

    if battery_num > 0:
        battery_installed = True
    else:
        battery_installed = False

    "find timezone from latitude and longitude"
    tf = timezonefinder.TimezoneFinder()
    timezone_str = tf.certain_timezone_at(lat=latitude, lng=longitude)

    # latitude, longitude, timezone, altitude, name(optional):
    if timezone_str == None:
        timezone_str = "America/New_York"
    tus = Location(latitude, longitude, timezone_str, altitude)
    times_loc = pd.date_range(start=datetime.datetime(2014, 1, 1), end=datetime.datetime(2014, 12, 31), freq='3600s',
                              tz=tus.tz)

    "initialization:"
    saved_energy = 0
    saved_energy_battery = 0
    battery_graph = []
    surface_tilt = tus.latitude
    solpos = pvlib.solarposition.get_solarposition(times_loc, tus.latitude, tus.longitude)
    dni_extra = pvlib.irradiance.extraradiation(times_loc)
    airmass = pvlib.atmosphere.relativeairmass(solpos['apparent_zenith'])
    pressure = pvlib.atmosphere.alt2pres(tus.altitude)
    am_abs = pvlib.atmosphere.absoluteairmass(airmass, pressure)
    cs = tus.get_clearsky(times_loc)
    progress = []

    if battery_installed == True:
        state_of_charge = 0  # state of charge of the battery
        battery_cap = rest_battery_cap_kwh(bat_kwh)

    aoi = pvlib.irradiance.aoi(surface_tilt, azimuth, solpos['apparent_zenith'], solpos['azimuth'])
    total_irrad = pvlib.irradiance.total_irrad(surface_tilt,
                                               azimuth,
                                               solpos['apparent_zenith'],
                                               solpos['azimuth'],
                                               cs['dni'], cs['ghi'], cs['dhi'],
                                               dni_extra=dni_extra,
                                               model='haydavies')
    # irradiance, wind, temp:
    temps = pvlib.pvsystem.sapm_celltemp(total_irrad['poa_global'], WIND_SPEED, TEMP_CELL)

    mins = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], dtype=float)
    maxs = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], dtype=float)

    def descale(x, min, max):
        if min == max:
            return max
        else:
            return min + x * (max - min)

    def generated_power(x):
        result = []
        nonlocal saved_energy
        nonlocal saved_energy_battery
        nonlocal azimuth
        nonlocal state_of_charge
        nonlocal battery_cap
        nonlocal battery_installed
        nonlocal battery_graph

        module_param = {'C0': x[0], 'C1': x[1], 'C2': x[2], 'C3': x[3], 'N': x[4], 'Impo': x[5], 'Aimp': x[6],
                        'Vmpo': x[7], 'Bvmpo': x[8], 'Cells_in_Series': x[9], 'A0': x[10], 'A1': x[11],
                        'A2': x[12], 'A3': x[13], 'A4': x[14], 'B0': x[15], 'B1': x[16], 'B2': x[17], 'B3': x[18],
                        'B4': x[19], 'B5': x[20], 'FD': x[21]}

        nonlocal temp_effective_irradiance
        temp_effective_irradiance = pvlib.pvsystem.sapm_effective_irradiance(
            total_irrad['poa_direct'], total_irrad['poa_diffuse'],
            am_abs, aoi, module_param)

        nonlocal effective_irradiance
        effective_irradiance = temp_effective_irradiance[:DAYS_NO * MEASUREMENTS_PER_DAY]

        for i in range(len(effective_irradiance)):
            if effective_irradiance[i] > 0:
                '''
                The credits for the following equations goes to Sandia National Laboratories, Albuquerque, NM.

                The Sandia PV Array Performance Model (SAPM) generates 5 points on a
                PV module's I-V curve (Voc, Isc, Ix, Ixx, Vmp/Imp) according to
                SAND2004-3535. Assumes a reference cell temperature of 25 C.

                References
                ----------
                [1] King, D. et al, 2004, "Sandia Photovoltaic Array Performance
                Model", SAND Report 3535, Sandia National Laboratories, Albuquerque,
                NM.
                '''

                C0 = x[0]
                C1 = x[1]
                C2 = x[2]
                C3 = x[3]
                N = x[4]
                Impo = x[5]
                Aimp = x[6]
                Vmpo = x[7]
                Bvmpo = x[8]
                Cells_in_Series = x[9]
                delta = N * kb * (TEMP_CELL + 273.15) / q

                i_mp = Impo * (C0 * effective_irradiance[i] + C1 * (effective_irradiance[i] ** 2)) * (
                        1 + Aimp * (TEMP_CELL - T0))
                v_mp = np.maximum(0, (Vmpo + C2 * Cells_in_Series * delta * np.log(effective_irradiance[i]) +
                                      C3 * Cells_in_Series * (
                                                  (delta * np.log(effective_irradiance[i])) ** 2) + Bvmpo * (
                                              TEMP_CELL - T0)))
                "[1]"

                result.append((i_mp * v_mp) / 1000 * pv_num)  # divide by 1000 to get kW from W

                "find the amount of saved energy by pv system:"
                if float(household_data[i]) > result[i]:
                    # if electricity consumption is bigger than electricity production by pv module
                    saved_energy += result[i]  # cover power consumption from pv module
                    if battery_installed:
                        remaining_consump = float(household_data[i]) - result[i]
                        if state_of_charge > 0:
                            if remaining_consump > state_of_charge * battery_cap:
                                saved_energy += state_of_charge * battery_cap
                                saved_energy_battery += state_of_charge * battery_cap
                                state_of_charge = discharge_battery(state_of_charge, remaining_consump, 1,
                                                                       battery_cap)
                            else:
                                saved_energy += remaining_consump  # cover power consumption from battery
                                saved_energy_battery += remaining_consump
                                state_of_charge = discharge_battery(state_of_charge, remaining_consump, 1,
                                                                       battery_cap)
                else:
                    saved_energy += float(household_data[i])  # cover power consumption from pv module
                    if battery_installed:
                        remaining_energy = result[i] - float(household_data[i])
                        if remaining_energy > 0:
                            if state_of_charge < 1:
                                state_of_charge = charge_battery(state_of_charge, remaining_energy, 1, battery_cap)
            else:
                if float(household_data[i]) > 0:
                    if battery_installed:
                        if state_of_charge > 0:
                            if float(household_data[i]) > state_of_charge * battery_cap:
                                saved_energy += state_of_charge * battery_cap
                                saved_energy_battery += state_of_charge * battery_cap
                                state_of_charge = discharge_battery(state_of_charge, float(household_data[i]), 1,
                                                                       battery_cap)
                            else:
                                saved_energy += float(household_data[i])  # cover power consumption from battery
                                saved_energy_battery += float(household_data[i])
                                state_of_charge = discharge_battery(state_of_charge, float(household_data[i]), 1,
                                                                       battery_cap)

                result.append(0)  # if there is no irradiance, append 0 output power
            if battery_installed:
                battery_graph.append(state_of_charge * battery_cap)

        return result  # number of watts of generated power by PV module as list of generated power per hour

    def objective_fun(x, verbose=True):
        nonlocal saved_energy
        saved_energy = 0  # intersection energy in kWh per every minute during one day
        nonlocal state_of_charge
        state_of_charge = 0
        nonlocal saved_energy_battery
        saved_energy_battery = 0
        nonlocal battery_graph
        battery_graph = []

        x = [descale(x_i, mins_i, maxs_i) for x_i, mins_i, maxs_i in zip(x, mins, maxs)]

        "find existing pv module from database that is the best fit to the computed parameters:"
        recommended_module = find_best_module(x, MODULES_PATH, mins, maxs)

        rec_module_name = recommended_module[22].replace(' ', '_').replace('-', '_').replace('.', '_').replace('(', '_') \
            .replace(')', '_').replace('[', '_').replace(']', '_').replace(':', '_').replace('+', '_').replace('/', '_') \
            .replace('"', '_').replace(',', '_')  # prepare the name of the module

        "find savings for recommended module and azimuth:"
        module = SANDIA_MODULES[rec_module_name]  # get module according to it's name

        aoi = pvlib.irradiance.aoi(surface_tilt, azimuth, solpos['apparent_zenith'], solpos['azimuth'])
        total_irrad = pvlib.irradiance.total_irrad(surface_tilt,
                                                   azimuth,
                                                   solpos['apparent_zenith'],
                                                   solpos['azimuth'],
                                                   cs['dni'], cs['ghi'], cs['dhi'],
                                                   dni_extra=dni_extra,
                                                   model='haydavies')
        # irradiance, wind, temp:
        temps = pvlib.pvsystem.sapm_celltemp(total_irrad['poa_global'], WIND_SPEED, TEMP_CELL)
        temp_effective_irradiance = pvlib.pvsystem.sapm_effective_irradiance(
            total_irrad['poa_direct'], total_irrad['poa_diffuse'],
            am_abs, aoi, module)
        effective_irradiance = temp_effective_irradiance[:DAYS_NO * MEASUREMENTS_PER_DAY]

        saved_energy = 0
        state_of_charge = 0
        saved_energy_battery = 0
        battery_graph = []
        x1 = (float(recommended_module[0]),
              float(recommended_module[1]),
              float(recommended_module[2]),
              float(recommended_module[3]),
              float(recommended_module[4]),
              float(recommended_module[5]),
              float(recommended_module[6]),
              float(recommended_module[7]),
              float(recommended_module[8]),
              float(recommended_module[9]),
              float(recommended_module[10]),
              float(recommended_module[11]),
              float(recommended_module[12]),
              float(recommended_module[13]),
              float(recommended_module[14]),
              float(recommended_module[15]),
              float(recommended_module[16]),
              float(recommended_module[17]),
              float(recommended_module[18]),
              float(recommended_module[19]),
              float(recommended_module[20]),
              float(recommended_module[21]),
              azimuth)
        result_axis_y = generated_power(x1)

        pv_power = round(np.amax(result_axis_y[:DAYS_NO * MEASUREMENTS_PER_DAY]), 3)

        dict = {}
        dict["battery_costs"] = get_battery_costs(battery_num)
        dict["pv_costs"] = get_pv_modules_costs(pv_power, pv_price, pv_num)
        dict["inverter_costs"] = get_inverter_costs(pv_power)
        dict["controller_costs"] = get_controller_costs(pv_power, battery_num)
        aux_dict = get_installation_costs(pitched_roof, pv_num, pv_power, battery_num)
        dict["pv_install"] = aux_dict["pv"]
        dict["system_install"] = aux_dict["system"]
        dict["battery_install"] = aux_dict["battery"]
        dict["controller_install"] = aux_dict["controller"]

        "compute the return of investments:"
        total_savings = saved_energy * el_price * SYSTEM_LIFETIME
        total_costs = dict["battery_costs"] + dict["pv_costs"] + dict["inverter_costs"] + \
                      dict["controller_costs"] + dict["pv_install"] + dict["system_install"] + dict["battery_install"] + \
                      dict["controller_install"]
        net_savings = total_savings - total_costs

        nonlocal progress
        progress.append(net_savings)
        if verbose:
            print("Iter.:", len(progress), "Val.:", progress[len(progress) - 1])

        return net_savings * (-1)

    def deap_objective_fun(x):
        return (objective_fun(x, verbose=False),)


    def find_mins_maxs():
        temp_sandia_modules = genfromtxt(MODULES_PATH, delimiter=',',
                                         dtype=str)  # dtype is string so that names are also read
        temp_sandia_modules = temp_sandia_modules[3:]  # cut the headers

        # initialization of minimums of parameters of pv modules, it is needed for clamping in find_fitness():
        mins[0] = float(temp_sandia_modules[0, 12])  # C0
        mins[1] = float(temp_sandia_modules[0, 13])  # C1
        mins[2] = float(temp_sandia_modules[0, 19])  # C2
        mins[3] = float(temp_sandia_modules[0, 20])  # C3
        mins[4] = float(temp_sandia_modules[0, 18])  # N
        mins[5] = float(temp_sandia_modules[0, 8])  # Impo
        mins[6] = float(temp_sandia_modules[0, 11])  # Aimp
        mins[7] = float(temp_sandia_modules[0, 9])  # Vmpo
        mins[8] = float(temp_sandia_modules[0, 16])  # Bvmpo
        mins[9] = float(temp_sandia_modules[0, 4])  # Cells in series
        mins[10] = float(temp_sandia_modules[0, 21])  # A0
        mins[11] = float(temp_sandia_modules[0, 22])  # A1
        mins[12] = float(temp_sandia_modules[0, 23])  # A2
        mins[13] = float(temp_sandia_modules[0, 24])  # A3
        mins[14] = float(temp_sandia_modules[0, 25])  # A4
        mins[15] = float(temp_sandia_modules[0, 26])  # B0
        mins[16] = float(temp_sandia_modules[0, 27])  # B1
        mins[17] = float(temp_sandia_modules[0, 28])  # B2
        mins[18] = float(temp_sandia_modules[0, 29])  # B3
        mins[19] = float(temp_sandia_modules[0, 30])  # B4
        mins[20] = float(temp_sandia_modules[0, 31])  # B5
        mins[21] = float(temp_sandia_modules[0, 33])  # FD

        # initialization of maximums of parameters of pv modules, it is needed for clamping in find_fitness():
        maxs[0] = float(temp_sandia_modules[0, 12])  # C0
        maxs[1] = float(temp_sandia_modules[0, 13])  # C1
        maxs[2] = float(temp_sandia_modules[0, 19])  # C2
        maxs[3] = float(temp_sandia_modules[0, 20])  # C3
        maxs[4] = float(temp_sandia_modules[0, 18])  # N
        maxs[5] = float(temp_sandia_modules[0, 8])  # Impo
        maxs[6] = float(temp_sandia_modules[0, 11])  # Aimp
        maxs[7] = float(temp_sandia_modules[0, 9])  # Vmpo
        maxs[8] = float(temp_sandia_modules[0, 16])  # Bvmpo
        maxs[9] = float(temp_sandia_modules[0, 4])  # Cells in series
        maxs[10] = float(temp_sandia_modules[0, 21])  # A0
        maxs[11] = float(temp_sandia_modules[0, 22])  # A1
        maxs[12] = float(temp_sandia_modules[0, 23])  # A2
        maxs[13] = float(temp_sandia_modules[0, 24])  # A3
        maxs[14] = float(temp_sandia_modules[0, 25])  # A4
        maxs[15] = float(temp_sandia_modules[0, 26])  # B0
        maxs[16] = float(temp_sandia_modules[0, 27])  # B1
        maxs[17] = float(temp_sandia_modules[0, 28])  # B2
        maxs[18] = float(temp_sandia_modules[0, 29])  # B3
        maxs[19] = float(temp_sandia_modules[0, 30])  # B4
        maxs[20] = float(temp_sandia_modules[0, 31])  # B5
        maxs[21] = float(temp_sandia_modules[0, 33])  # FD

        for i in range(1, DB_LEN):
            # find minimums:
            if mins[0] > float(temp_sandia_modules[i, 12]):
                mins[0] = float(temp_sandia_modules[i, 12])
            if mins[1] > float(temp_sandia_modules[i, 13]):
                mins[1] = float(temp_sandia_modules[i, 13])
            if mins[2] > float(temp_sandia_modules[i, 19]):
                mins[2] = float(temp_sandia_modules[i, 19])
            if mins[3] > float(temp_sandia_modules[i, 20]):
                mins[3] = float(temp_sandia_modules[i, 20])
            if mins[4] > float(temp_sandia_modules[i, 18]):
                mins[4] = float(temp_sandia_modules[i, 18])
            if mins[5] > float(temp_sandia_modules[i, 8]):
                mins[5] = float(temp_sandia_modules[i, 8])
            if mins[6] > float(temp_sandia_modules[i, 11]):
                mins[6] = float(temp_sandia_modules[i, 11])
            if mins[7] > float(temp_sandia_modules[i, 9]):
                mins[7] = float(temp_sandia_modules[i, 9])
            if mins[8] > float(temp_sandia_modules[i, 16]):
                mins[8] = float(temp_sandia_modules[i, 16])
            if mins[9] > float(temp_sandia_modules[i, 4]):
                mins[9] = float(temp_sandia_modules[i, 4])
            if mins[10] > float(temp_sandia_modules[i, 21]):
                mins[10] = float(temp_sandia_modules[i, 21])
            if mins[11] > float(temp_sandia_modules[i, 22]):
                mins[11] = float(temp_sandia_modules[i, 22])
            if mins[12] > float(temp_sandia_modules[i, 23]):
                mins[12] = float(temp_sandia_modules[i, 23])
            if mins[13] > float(temp_sandia_modules[i, 24]):
                mins[13] = float(temp_sandia_modules[i, 24])
            if mins[14] > float(temp_sandia_modules[i, 25]):
                mins[14] = float(temp_sandia_modules[i, 25])
            if mins[15] > float(temp_sandia_modules[i, 26]):
                mins[15] = float(temp_sandia_modules[i, 26])
            if mins[16] > float(temp_sandia_modules[i, 27]):
                mins[16] = float(temp_sandia_modules[i, 27])
            if mins[17] > float(temp_sandia_modules[i, 28]):
                mins[17] = float(temp_sandia_modules[i, 28])
            if mins[18] > float(temp_sandia_modules[i, 29]):
                mins[18] = float(temp_sandia_modules[i, 29])
            if mins[19] > float(temp_sandia_modules[i, 30]):
                mins[19] = float(temp_sandia_modules[i, 30])
            if mins[20] > float(temp_sandia_modules[i, 31]):
                mins[20] = float(temp_sandia_modules[i, 31])
            if mins[21] > float(temp_sandia_modules[i, 33]):
                mins[21] = float(temp_sandia_modules[i, 33])

            # find maximums:
            if maxs[0] < float(temp_sandia_modules[i, 12]):
                maxs[0] = float(temp_sandia_modules[i, 12])
            if maxs[1] < float(temp_sandia_modules[i, 13]):
                maxs[1] = float(temp_sandia_modules[i, 13])
            if maxs[2] < float(temp_sandia_modules[i, 19]):
                maxs[2] = float(temp_sandia_modules[i, 19])
            if maxs[3] < float(temp_sandia_modules[i, 20]):
                maxs[3] = float(temp_sandia_modules[i, 20])
            if maxs[4] < float(temp_sandia_modules[i, 18]):
                maxs[4] = float(temp_sandia_modules[i, 18])
            if maxs[5] < float(temp_sandia_modules[i, 8]):
                maxs[5] = float(temp_sandia_modules[i, 8])
            if maxs[6] < float(temp_sandia_modules[i, 11]):
                maxs[6] = float(temp_sandia_modules[i, 11])
            if maxs[7] < float(temp_sandia_modules[i, 9]):
                maxs[7] = float(temp_sandia_modules[i, 9])
            if maxs[8] < float(temp_sandia_modules[i, 16]):
                maxs[8] = float(temp_sandia_modules[i, 16])
            if maxs[9] < float(temp_sandia_modules[i, 4]):
                maxs[9] = float(temp_sandia_modules[i, 4])
            if maxs[10] < float(temp_sandia_modules[i, 21]):
                maxs[10] = float(temp_sandia_modules[i, 21])
            if maxs[11] < float(temp_sandia_modules[i, 22]):
                maxs[11] = float(temp_sandia_modules[i, 22])
            if maxs[12] < float(temp_sandia_modules[i, 23]):
                maxs[12] = float(temp_sandia_modules[i, 23])
            if maxs[13] < float(temp_sandia_modules[i, 24]):
                maxs[13] = float(temp_sandia_modules[i, 24])
            if maxs[14] < float(temp_sandia_modules[i, 25]):
                maxs[14] = float(temp_sandia_modules[i, 25])
            if maxs[15] < float(temp_sandia_modules[i, 26]):
                maxs[15] = float(temp_sandia_modules[i, 26])
            if maxs[16] < float(temp_sandia_modules[i, 27]):
                maxs[16] = float(temp_sandia_modules[i, 27])
            if maxs[17] < float(temp_sandia_modules[i, 28]):
                maxs[17] = float(temp_sandia_modules[i, 28])
            if maxs[18] < float(temp_sandia_modules[i, 29]):
                maxs[18] = float(temp_sandia_modules[i, 29])
            if maxs[19] < float(temp_sandia_modules[i, 30]):
                maxs[19] = float(temp_sandia_modules[i, 30])
            if maxs[20] < float(temp_sandia_modules[i, 31]):
                maxs[20] = float(temp_sandia_modules[i, 31])
            if maxs[21] < float(temp_sandia_modules[i, 33]):
                maxs[21] = float(temp_sandia_modules[i, 33])

    "find boundaries from database of pv modules:"
    find_mins_maxs()

    "bounds:"
    # x0 is initial guess (last value is surface azimuth)
    bnds = ((0, 1), ) * 22

    x0 = np.empty(22)
    x0.fill(0.5)

    from deap import creator, base, cma, tools, algorithms

    creator.create("FitnessMin", base.Fitness, weights=(-1,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    toolbox = base.Toolbox()
    toolbox.register('evaluate', deap_objective_fun)

    strategy = cma.Strategy(centroid=[.5]*len(bnds), sigma=.1)
    toolbox.register('generate', strategy.generate, creator.Individual)
    toolbox.register('update', strategy.update)

    hof = tools.HallOfFame(1)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    algorithms.eaGenerateUpdate(toolbox, ngen=10, stats=stats, halloffame=hof)

    print(f'Finished, best parameters: {hof[0]}, obj. val.: {objective_fun(hof[0])}')

    x = [descale(solx, mi, ma) for solx, mi, ma in zip(hof[0], mins, maxs)]

    "find existing pv module from database that is the best fit to the computed parameters:"
    recommended_module = find_best_module(x, MODULES_PATH, mins, maxs)

    rec_module_name = recommended_module[22].replace(' ', '_').replace('-', '_').replace('.', '_').replace('(', '_') \
        .replace(')', '_').replace('[', '_').replace(']', '_').replace(':', '_').replace('+', '_').replace('/', '_') \
        .replace('"', '_').replace(',', '_')  # prepare the name of the module

    "find savings for recommended module and azimuth:"
    module = SANDIA_MODULES[rec_module_name]  # get module according to it's name

    aoi = pvlib.irradiance.aoi(surface_tilt, azimuth, solpos['apparent_zenith'], solpos['azimuth'])
    total_irrad = pvlib.irradiance.total_irrad(surface_tilt,
                                               azimuth,
                                               solpos['apparent_zenith'],
                                               solpos['azimuth'],
                                               cs['dni'], cs['ghi'], cs['dhi'],
                                               dni_extra=dni_extra,
                                               model='haydavies')
    # irradiance, wind, temp:
    temps = pvlib.pvsystem.sapm_celltemp(total_irrad['poa_global'], WIND_SPEED, TEMP_CELL)
    temp_effective_irradiance = pvlib.pvsystem.sapm_effective_irradiance(
        total_irrad['poa_direct'], total_irrad['poa_diffuse'],
        am_abs, aoi, module)
    effective_irradiance = temp_effective_irradiance[:DAYS_NO * MEASUREMENTS_PER_DAY]

    saved_energy = 0
    state_of_charge = 0
    saved_energy_battery = 0
    battery_graph = []
    x1 = (float(recommended_module[0]),
          float(recommended_module[1]),
          float(recommended_module[2]),
          float(recommended_module[3]),
          float(recommended_module[4]),
          float(recommended_module[5]),
          float(recommended_module[6]),
          float(recommended_module[7]),
          float(recommended_module[8]),
          float(recommended_module[9]),
          float(recommended_module[10]),
          float(recommended_module[11]),
          float(recommended_module[12]),
          float(recommended_module[13]),
          float(recommended_module[14]),
          float(recommended_module[15]),
          float(recommended_module[16]),
          float(recommended_module[17]),
          float(recommended_module[18]),
          float(recommended_module[19]),
          float(recommended_module[20]),
          float(recommended_module[21]))
    result_axis_y = generated_power(x1)

    "result summarization for recommended pv module:"
    module_energy = 0
    household_demand = 0
    for i in range(DAYS_NO * MEASUREMENTS_PER_DAY):
        module_energy += result_axis_y[i]
        household_demand += household_data[i]

    pv_power = round(np.amax(result_axis_y[:DAYS_NO * MEASUREMENTS_PER_DAY]), 3)

    dict = {}
    dict["pv_name"] = recommended_module[22]
    dict["timezone"] = timezone_str
    dict["annual_generated"] = round(module_energy, 3)
    dict["annual_consumption"] = round(household_demand, 3)
    dict["cov_system"] = round(saved_energy, 3)
    dict["cov_pv"] = round(saved_energy - saved_energy_battery, 3)
    dict["cov_battery"] = round(saved_energy_battery, 3)
    dict["annual_savings"] = round(saved_energy * el_price, 2)
    dict["max_power"] = pv_power
    dict["max_consumption"] = round(np.amax(household_data[:DAYS_NO * MEASUREMENTS_PER_DAY]), 3)
    dict["battery_rep_num"] = get_battery_replacements(battery_num)
    dict["battery_costs"] = get_battery_costs(battery_num)
    dict["pv_costs"] = get_pv_modules_costs(pv_power, pv_price, pv_num)
    dict["inverter_costs"] = get_inverter_costs(pv_power)
    dict["controller_costs"] = get_controller_costs(pv_power, battery_num)
    aux_dict = get_installation_costs(pitched_roof, pv_num, pv_power, battery_num)
    dict["pv_install"] = aux_dict["pv"]
    dict["system_install"] = aux_dict["system"]
    dict["battery_install"] = aux_dict["battery"]
    dict["controller_install"] = aux_dict["controller"]

    "compute the return of investments:"
    total_savings = round(saved_energy * el_price, 2) * SYSTEM_LIFETIME
    total_costs = dict["battery_costs"] + dict["pv_costs"] + dict["inverter_costs"] + \
                  dict["controller_costs"] + dict["pv_install"] + dict["system_install"] + dict["battery_install"] + \
                  dict["controller_install"]
    total_costs = round(total_costs, 2)
    net_savings = total_savings - total_costs
    net_savings = round(net_savings, 2)

    dict["total_savings"] = total_savings
    dict["total_costs"] = total_costs
    dict["net_savings"] = net_savings

    if (net_savings > 0):
        dict["pay_back"] = True
    else:
        dict["pay_back"] = False

    return dict
