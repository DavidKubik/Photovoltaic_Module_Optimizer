"""
 Copyright (C) Dávid Kubík - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
 Written by Dávid Kubík <kubik.david5@gmail.com>, March 2018
"""

"constants:"
REST_L = 0.2  # lower restriction of battery charge level
REST_H = 0.8  # upper restriction of battery charge
LOSS = 1.0526  # the loss of energy caused by battery storage

def rest_battery_cap_ah(c_ah, voltage):
    """
    Computes restricted battery capacity in watt hour according to capacity in ampere hour and voltage of the battery.
    Because of the ageing of the battery, it is assumed that 90% of the usable battery capacity is indeed utilised
    within the lifetime on average. Therefore we multiply battery capacity by 0.9
    To extend the battery life to the maximum, we restricted the state of charge of the battery to a range between
    20% and 80% of the nominal battery capacity.
    """

    c_wh = 0.9 * c_ah * voltage

    return (c_wh * (1 - (REST_L + (1 - REST_H)))) / 1000  # restricted battery capacity in kilowatt hour

def rest_battery_cap_kwh(c_kwh):
    """
    Computes restricted battery capacity in kilowatt hour according to battery capacity in kilowatt hour.
    Because of the ageing of the battery, it is assumed that 90% of the usable battery capacity is indeed utilised
    within the lifetime on average. Therefore we multiply battery capacity by 0.9
    To extend the battery life to the maximum, we restricted the state of charge of the battery to a range between
    20% and 80% of the nominal battery capacity.
    """

    return 0.9 * c_kwh * (1 - (REST_L + (1 - REST_H)))  # restricted battery capacity in watt hour


def charge_battery(state_of_charge, kilowatts, hours, battery_cap):
    """
    Returns new state of charge after the battery was charged
    :param hours represents time the battery have been charged
    """

    new_state = state_of_charge + ((kilowatts * hours) / battery_cap)
    if new_state > 1:
        return 1
    else:
        return new_state


def discharge_battery(state_of_charge, kilowatts, hours, battery_cap):
    """
    Returns new state of charge after the battery was discharged
    :param hours represents time the battery have been discharged
    """

    new_state = state_of_charge - ((kilowatts * hours * LOSS) / battery_cap)
    if new_state < 0:
        return 0
    else:
        return new_state
