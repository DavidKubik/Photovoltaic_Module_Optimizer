from pv_module_optimizer import compute

import time
import numpy as np

start = time.time()

# USA - Dallas:
# FILE_RESULT_NAME = "../results/result_dallas_combinations.csv"
# DATA_SOURCE = "../data/average_household_usage_dallas.csv"
# latitude = 32.760564
# longitude = -96.825431
# altitude = 138
# el_price = 0.1016  # https://www.bounceenergy.com/ DOLLAR = 0.8470 EUR

# USA - Austin:
FILE_RESULT_NAME = "../results/result_austin_combinations.csv"
DATA_SOURCE = "../data/average_household_usage_austin.csv"
latitude = 30.268076
longitude = -97.742220
altitude = 162
el_price = 0.1016  # https://www.bounceenergy.com/ DOLLAR = 0.8470 EUR

# Ireland - Dublin:
# FILE_RESULT_NAME = "../results/result_ireland_combinations.csv"
# DATA_SOURCE = "../data/average_household_usage_ireland.csv"
# latitude = 53.359151
# longitude = -6.280032
# altitude = 40
# el_price = 0.1734  # https://www.pandapower.ie/plan/cashback-bundle-electricity/

azimuth = 180
pitched_roof = True
pv_price = 0.65

with open(FILE_RESULT_NAME, "w") as file:
    file.write("pv_num,battery_num,pv_name,timezone,annual_generated,annual_consumption,cov_system,"
               "cov_pv,cov_battery,annual_savings,max_power,max_consumption,battery_rep_num,battery_costs,pv_costs,"
               "inverter_costs,controller_costs,pv_install,system_install,battery_install,controller_install,"
               "total_savings,total_costs,net_savings,pay_back\n")
file.close()

pv = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
battery = np.array([0, 1, 2, 3, 4, 5])

progress = 0
sum = 0

for pv_num in pv:
    for battery_num in battery:
        start_house = time.time()

        dict = compute(latitude, longitude, altitude, pv_num, battery_num, azimuth, pitched_roof,
                el_price, pv_price, use_data_from_file=True, file_path=DATA_SOURCE)

        with open(FILE_RESULT_NAME, "a") as file:
            file.write("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},"
                       "{21},{22},{23},{24}\n".format(pv_num,
                                                 battery_num,
                                                 dict["pv_name"],
                                                 dict["timezone"],
                                                 dict["annual_generated"],
                                                 dict["annual_consumption"],
                                                 dict["cov_system"],
                                                 dict["cov_pv"],
                                                 dict["cov_battery"],
                                                 dict["annual_savings"],
                                                 dict["max_power"],
                                                 dict["max_consumption"],
                                                 dict["battery_rep_num"],
                                                 dict["battery_costs"],
                                                 dict["pv_costs"],
                                                 dict["inverter_costs"],
                                                 dict["controller_costs"],
                                                 dict["pv_install"],
                                                 dict["system_install"],
                                                 dict["battery_install"],
                                                 dict["controller_install"],
                                                 dict["total_savings"],
                                                 dict["total_costs"],
                                                 dict["net_savings"],
                                                 dict["pay_back"]))
        file.close()
        print(dict["pv_name"])
        print(dict["timezone"])

        progress += 1
        end_house = time.time()
        sum += end_house - start_house
        house_time = sum / progress

        print("PROGRESS: {0}/{1} ~ {2}%    ESTIMATED REMAINING TIME: {3} hour {4} min {5} sec\n\n".format(
            progress,
            60,
            round((progress / 60) * 100, 2),
            int(((60 - progress) * house_time) / 3600),
            int((((60 - progress) * house_time) % 3600) / 60),
            round(((60 - progress) * house_time) % 60)))

end = time.time()
print("\nTesting household all combinations finished in:", int((end - start) / 60), "min", round((end - start) % 60, 1),
      "sec")
