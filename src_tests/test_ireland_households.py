from pv_module_optimizer import compute

import time

start = time.time()

FILE_FIRST = 1414  # name of the first file
FILE_LAST = 1414  # name of the last file
NEW_FILE_NAME = "household_usage_ireland_"
FILE_RESULT_NAME = "../results/result_ireland_households.csv"

pv_num = 9
battery_num = 2
azimuth = 180
pitched_roof = True
el_price = 0.1734  # https://www.pandapower.ie/plan/cashback-bundle-electricity/
pv_price = 0.65
file_name = FILE_FIRST
file_count = 0  # we need to count number of files, because there are missing files
sum = 0

with open(FILE_RESULT_NAME, "w") as file:
    file.write("household_num,pv_name,timezone,annual_generated,annual_consumption,cov_system,cov_pv,cov_battery,"
               "annual_savings,max_power,max_consumption,battery_rep_num,battery_costs,pv_costs,inverter_costs,"
               "controller_costs,pv_install,system_install,battery_install,controller_install,total_savings,"
               "total_costs,net_savings,pay_back\n")
file.close()

while file_name <= FILE_LAST:
    try:
        start_house = time.time()

        # Ireland, Dublin
        latitude = 53.359151
        longitude = -6.280032
        altitude = 40

        dict = compute(latitude, longitude, altitude, pv_num, battery_num, azimuth, pitched_roof,
                el_price, pv_price, use_data_from_file=True, file_path="../data/ireland_households/" + NEW_FILE_NAME +
                                                                       str(file_name) + ".csv")

        with open(FILE_RESULT_NAME, "a") as file:
            file.write("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},"
                       "{21},{22},{23}\n".format(file_name,
                                                 dict["pv_name"],
                                                 dict["timezone"],
                                                 dict["annual_generated"],
                                                 dict["annual_consumption"],
                                                 dict["cov_system"],
                                                 dict["cov_pv"],
                                                 dict["cov_battery"],
                                                 dict["annual_savings"],
                                                 dict["max_power"],
                                                 dict["max_consumption"],
                                                 dict["battery_rep_num"],
                                                 dict["battery_costs"],
                                                 dict["pv_costs"],
                                                 dict["inverter_costs"],
                                                 dict["controller_costs"],
                                                 dict["pv_install"],
                                                 dict["system_install"],
                                                 dict["battery_install"],
                                                 dict["controller_install"],
                                                 dict["total_savings"],
                                                 dict["total_costs"],
                                                 dict["net_savings"],
                                                 dict["pay_back"]))
        file.close()

        file_count += 1
        file_name += 1

        end_house = time.time()
        sum += end_house - start_house
        house_time = sum / file_count

        print("PROGRESS: {0}/{1} ~ {2}%    ESTIMATED REMAINING TIME: {3} hour {4} min {5} sec\n\n".format(
            file_count,
            709,
            round((file_count / 709) * 100, 2),
            int(((709 - file_count) * house_time) / 3600),
            int((((709 - file_count) * house_time) % 3600) / 60),
            round(((709 - file_count) * house_time) % 60)))

    except IOError:
        file_name += 1

end = time.time()
print("\nTesting household locations finished in:", int((end - start) / 60), "min", round((end - start) % 60, 1), "sec")
