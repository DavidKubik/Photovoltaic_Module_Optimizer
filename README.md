# Photovoltaic_Module_Optimizer
We propose a method for finding the most appropriate photovoltaic (hereafter PV) module and the return on investment for specific household needs, leveraging mathematical optimization. Based on electricity consumption and location of the household, the algorithm finds PV module design parameters using Covariance Matrix Adaptation Evolution Strategy. According to these computed design parameters, the algorithm finds the most similar PV module from the dataset of PV modules using Euclidean distance. Subsequently, savings and costs are calculated for the recommended PV system. When calculating the return on investment, the algorithm takes into account the price of electricity, initial investment and battery replacements. The objective function is defined to maximize net savings during the lifetime of the PV system, which is considered to be 20 years. Heuristics of the battery takes into account the aging of batteries and the loss of energy due to storage. Battery life is set at 10 years, as we are considering using high quality batteries. In order to extend battery life, the algorithm counts with the charge level restrictions.

## Copyright
Copyright © 2017 Dávid Kubík. All Rights Reserved.

NOTICE:
 *  All information contained herein, that are not parts of pvlib source code, are and remains the property of Dávid Kubík.
 *  The intellectual and technical concepts contained herein are proprietary to Dávid Kubík.
 *  Dissemination of the information, or reproduction of this material, that do not contains parts of pvlib source code, without author permission is strictly forbidden.

## Prerequisities
```
pip install -r requirements.txt
```

## Example request

Run localhost:
```
cd src_server
python flask_server.py
```

Send request with data:
```
curl -X POST http://0.0.0.0:5000/compute -d @json.txt --header "Content-Type: application/json"
```
