package sk.stuba.fiit.kubikd.photovoltaic_module_optimizer_client;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;

/**
 * Created by David on 4.4.2018.
 */

public class OutputActivity extends Activity {

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_output);

        String jsonMyObject = null;
        Result myObject = null; //TODO zmenit Result triedu tak aby prijimala udaje zo serveru

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            jsonMyObject = extras.getString("result");
        }

        if (jsonMyObject != null) {
            myObject = new Gson().fromJson(jsonMyObject, Result.class);
        }

        TextView pvNameValTxtV = (TextView) findViewById(R.id.pvNameValTxtV);
        TextView timezoneValTxtV = (TextView) findViewById(R.id.timezoneValTxtV);
        TextView genElValTxtV = (TextView) findViewById(R.id.genElValTxtV);
        TextView maxPvValTxtV = (TextView) findViewById(R.id.maxPvValTxtV);
        TextView demandValTxtV = (TextView) findViewById(R.id.demandValTxtV);
        TextView maxDemandValTxtV = (TextView) findViewById(R.id.maxDemandValTxtV);
        TextView covSysValTxtV = (TextView) findViewById(R.id.covSysValTxtV);
        TextView covPvValTxtV = (TextView) findViewById(R.id.covPvValTxtV);
        TextView covBatValTxtV = (TextView) findViewById(R.id.covBatValTxtV);
        TextView savingsValTxtV = (TextView) findViewById(R.id.savingsValTxtV);
        TextView pvCostsValTxtV = (TextView) findViewById(R.id.pvCostsValTxtV);
        TextView inverterCostsValTxtV = (TextView) findViewById(R.id.inverterCostsValTxtV);
        TextView batteryCostsValTxtV = (TextView) findViewById(R.id.batteryCostsValTxtV);
        TextView replacementsValTxtV = (TextView) findViewById(R.id.replacementsValTxtV);
        TextView controllerValTxtV = (TextView) findViewById(R.id.controllerValTxtV);
        TextView pvInstallValTxtV = (TextView) findViewById(R.id.pvInstallValTxtV);
        TextView batteryInstallValTxtV = (TextView) findViewById(R.id.batteryInstallValTxtV);
        TextView controllerInstallValTxtV = (TextView) findViewById(R.id.controllerInstallValTxtV);
        TextView otherInstallValTxtV = (TextView) findViewById(R.id.otherInstallValTxtV);
        TextView totalSavingsValTxtV = (TextView) findViewById(R.id.totalSavingsValTxtV);
        TextView totalCostsValTxtV = (TextView) findViewById(R.id.totalCostsValTxtV);
        TextView netSavingsValTxtV = (TextView) findViewById(R.id.netSavingsValTxtV);
        TextView investmentRetValTxtV = (TextView) findViewById(R.id.investmentRetValTxtV);

        if (myObject != null) {
            pvNameValTxtV.setText(myObject.getPvName());
            timezoneValTxtV.setText(myObject.getTimezone());
            genElValTxtV.setText(Float.toString(myObject.getAnnualGenerated()));
            maxPvValTxtV.setText(Float.toString(myObject.getMaxPower()));
            demandValTxtV.setText(Float.toString(myObject.getAnnualConsumption()));
            maxDemandValTxtV.setText(Float.toString(myObject.getMaxConsumption()));
            covSysValTxtV.setText(Float.toString(myObject.getCovSystem()));
            covPvValTxtV.setText(Float.toString(myObject.getCovPv()));
            covBatValTxtV.setText(Float.toString(myObject.getCovBattery()));
            savingsValTxtV.setText(Float.toString(myObject.getAnnualSavings()));
            pvCostsValTxtV.setText(Float.toString(myObject.getPvCosts()));
            inverterCostsValTxtV.setText(Float.toString(myObject.getInverterCosts()));
            batteryCostsValTxtV.setText(Float.toString(myObject.getBatteryCosts()));
            replacementsValTxtV.setText(Integer.toString(myObject.getBatteryRepNum() - 1));
            controllerValTxtV.setText(Float.toString(myObject.getControllerCosts()));
            pvInstallValTxtV.setText(Float.toString(myObject.getPvInstall()));
            batteryInstallValTxtV.setText(Float.toString(myObject.getBatteryInstall()));
            controllerInstallValTxtV.setText(Float.toString(myObject.getControllerInstall()));
            otherInstallValTxtV.setText(Float.toString(myObject.getSystemInstall()));
            totalSavingsValTxtV.setText(Float.toString(myObject.getTotalSavings()));
            totalCostsValTxtV.setText(Float.toString(myObject.getTotalCosts()));
            netSavingsValTxtV.setText(Float.toString(myObject.getNetSavings()));
            if (myObject.isPayBack()) {
                investmentRetValTxtV.setText(R.string.yes);
            } else {
                investmentRetValTxtV.setText(R.string.no);
            }
        }
    }
}
