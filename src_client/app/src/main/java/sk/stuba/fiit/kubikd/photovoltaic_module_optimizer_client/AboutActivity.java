package sk.stuba.fiit.kubikd.photovoltaic_module_optimizer_client;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

/**
 * Created by David on 4.4.2018.
 */

public class AboutActivity extends Activity {

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_about);
    }
}
