package sk.stuba.fiit.kubikd.photovoltaic_module_optimizer_client;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.List;

/**
 * Created by David on 4.4.2018.
 */

public class MyJson {

    private float latitude;
    private float longitude;
    private float altitude;
    private int pvNum;
    private int batteryNum;
    private float azimuth;
    private boolean pitchedRoof;
    private float elPrice;
    private float pvPrice;
    private boolean useWeekend;
    private List<Float> workday;
    private List<Float> weekend;

    MyJson(float latitude, float longitude, float altitude, int pvNum, int batteryNum,
           float azimuth, boolean pitchedRoof, float elPrice, float pvPrice,
           boolean useWeekend, List<Float> workday,
           List<Float> weekend) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.pvNum = pvNum;
        this.batteryNum = batteryNum;
        this.azimuth = azimuth;
        this.pitchedRoof = pitchedRoof;
        this.elPrice = elPrice;
        this.pvPrice = pvPrice;
        this.useWeekend = useWeekend;
        this.workday = workday;
        this.weekend = weekend;
    }

    public ArrayNode getJson() {
        ObjectMapper mapper = new ObjectMapper();

        ArrayNode arrayNode = mapper.createArrayNode();

        ObjectNode objectNode = mapper.createObjectNode();

        objectNode.put("latitude", latitude);
        objectNode.put("longitude", longitude);
        objectNode.put("altitude", altitude);
        objectNode.put("pvNum", pvNum);
        objectNode.put("batteryNum", batteryNum);
        objectNode.put("azimuth", azimuth);
        objectNode.put("pitchedRoof", pitchedRoof);
        objectNode.put("elPrice", elPrice);
        objectNode.put("pvPrice", pvPrice);
        objectNode.put("useWeekend", useWeekend);

        //workday:
        ArrayNode workdayAN = mapper.createArrayNode();

        ObjectNode zero = mapper.createObjectNode();
        zero.put("zero", workday.get(0));
        Log.i("MyJson", "zero " + zero.get("zero"));

        ObjectNode one = mapper.createObjectNode();
        one.put("one", workday.get(1));

        ObjectNode two = mapper.createObjectNode();
        two.put("two", workday.get(2));

        ObjectNode three = mapper.createObjectNode();
        three.put("three", workday.get(3));

        ObjectNode four = mapper.createObjectNode();
        four.put("four", workday.get(4));

        ObjectNode five = mapper.createObjectNode();
        five.put("five", workday.get(5));

        ObjectNode six = mapper.createObjectNode();
        six.put("six", workday.get(6));

        ObjectNode seven = mapper.createObjectNode();
        seven.put("seven", workday.get(7));

        ObjectNode eight = mapper.createObjectNode();
        eight.put("eight", workday.get(8));

        ObjectNode nine = mapper.createObjectNode();
        nine.put("nine", workday.get(9));

        ObjectNode ten = mapper.createObjectNode();
        ten.put("ten", workday.get(10));

        ObjectNode eleven = mapper.createObjectNode();
        eleven.put("eleven", workday.get(11));

        ObjectNode twelve = mapper.createObjectNode();
        twelve.put("twelve", workday.get(12));

        ObjectNode thirteen = mapper.createObjectNode();
        thirteen.put("thirteen", workday.get(13));

        ObjectNode fourteen = mapper.createObjectNode();
        fourteen.put("fourteen", workday.get(14));

        ObjectNode fifteen = mapper.createObjectNode();
        fifteen.put("fifteen", workday.get(15));

        ObjectNode sixteen = mapper.createObjectNode();
        sixteen.put("sixteen", workday.get(16));

        ObjectNode seventeen = mapper.createObjectNode();
        seventeen.put("seventeen", workday.get(17));

        ObjectNode eighteen = mapper.createObjectNode();
        eighteen.put("eighteen", workday.get(18));

        ObjectNode nineteen = mapper.createObjectNode();
        nineteen.put("nineteen", workday.get(19));

        ObjectNode twenty = mapper.createObjectNode();
        twenty.put("twenty", workday.get(20));

        ObjectNode twentyone = mapper.createObjectNode();
        twentyone.put("twentyone", workday.get(21));

        ObjectNode twentytwo = mapper.createObjectNode();
        twentytwo.put("twentytwo", workday.get(22));

        ObjectNode twentythree = mapper.createObjectNode();
        twentythree.put("twentythree", workday.get(23));

        workdayAN.add(zero);
        workdayAN.add(one);
        workdayAN.add(two);
        workdayAN.add(three);
        workdayAN.add(four);
        workdayAN.add(five);
        workdayAN.add(six);
        workdayAN.add(seven);
        workdayAN.add(eight);
        workdayAN.add(nine);
        workdayAN.add(ten);
        workdayAN.add(eleven);
        workdayAN.add(twelve);
        workdayAN.add(thirteen);
        workdayAN.add(fourteen);
        workdayAN.add(fifteen);
        workdayAN.add(sixteen);
        workdayAN.add(seventeen);
        workdayAN.add(eighteen);
        workdayAN.add(nineteen);
        workdayAN.add(twenty);
        workdayAN.add(twentyone);
        workdayAN.add(twentytwo);
        workdayAN.add(twentythree);

        ObjectNode workday2 = mapper.createObjectNode();
        workday2.putPOJO("workday", workdayAN);

        //weekend:
        ArrayNode weekendAN = mapper.createArrayNode();

        ObjectNode zero2 = mapper.createObjectNode();
        zero2.put("zero", weekend.get(0));

        ObjectNode one2 = mapper.createObjectNode();
        one2.put("one", weekend.get(1));

        ObjectNode two2 = mapper.createObjectNode();
        two2.put("two", weekend.get(2));

        ObjectNode three2 = mapper.createObjectNode();
        three2.put("three", weekend.get(3));

        ObjectNode four2 = mapper.createObjectNode();
        four2.put("four", weekend.get(4));

        ObjectNode five2 = mapper.createObjectNode();
        five2.put("five", weekend.get(5));

        ObjectNode six2 = mapper.createObjectNode();
        six2.put("six", weekend.get(6));

        ObjectNode seven2 = mapper.createObjectNode();
        seven2.put("seven", weekend.get(7));

        ObjectNode eight2 = mapper.createObjectNode();
        eight2.put("eight", weekend.get(8));

        ObjectNode nine2 = mapper.createObjectNode();
        nine2.put("nine", weekend.get(9));

        ObjectNode ten2 = mapper.createObjectNode();
        ten2.put("ten", weekend.get(10));

        ObjectNode eleven2 = mapper.createObjectNode();
        eleven2.put("eleven", weekend.get(11));

        ObjectNode twelve2 = mapper.createObjectNode();
        twelve2.put("twelve", weekend.get(12));

        ObjectNode thirteen2 = mapper.createObjectNode();
        thirteen2.put("thirteen", weekend.get(13));

        ObjectNode fourteen2 = mapper.createObjectNode();
        fourteen2.put("fourteen", weekend.get(14));

        ObjectNode fifteen2 = mapper.createObjectNode();
        fifteen2.put("fifteen", weekend.get(15));

        ObjectNode sixteen2 = mapper.createObjectNode();
        sixteen2.put("sixteen", weekend.get(16));

        ObjectNode seventeen2 = mapper.createObjectNode();
        seventeen2.put("seventeen", weekend.get(17));

        ObjectNode eighteen2 = mapper.createObjectNode();
        eighteen2.put("eighteen", weekend.get(18));

        ObjectNode nineteen2 = mapper.createObjectNode();
        nineteen2.put("nineteen", weekend.get(19));

        ObjectNode twenty2 = mapper.createObjectNode();
        twenty2.put("twenty", weekend.get(20));

        ObjectNode twentyone2 = mapper.createObjectNode();
        twentyone2.put("twentyone", weekend.get(21));

        ObjectNode twentytwo2 = mapper.createObjectNode();
        twentytwo2.put("twentytwo", weekend.get(22));

        ObjectNode twentythree2 = mapper.createObjectNode();
        twentythree2.put("twentythree", weekend.get(23));

        weekendAN.add(zero2);
        weekendAN.add(one2);
        weekendAN.add(two2);
        weekendAN.add(three2);
        weekendAN.add(four2);
        weekendAN.add(five2);
        weekendAN.add(six2);
        weekendAN.add(seven2);
        weekendAN.add(eight2);
        weekendAN.add(nine2);
        weekendAN.add(ten2);
        weekendAN.add(eleven2);
        weekendAN.add(twelve2);
        weekendAN.add(thirteen2);
        weekendAN.add(fourteen2);
        weekendAN.add(fifteen2);
        weekendAN.add(sixteen2);
        weekendAN.add(seventeen2);
        weekendAN.add(eighteen2);
        weekendAN.add(nineteen2);
        weekendAN.add(twenty2);
        weekendAN.add(twentyone2);
        weekendAN.add(twentytwo2);
        weekendAN.add(twentythree2);

        ObjectNode weekend2 = mapper.createObjectNode();
        weekend2.putPOJO("weekend", weekendAN);

        arrayNode.add(objectNode);
        arrayNode.add(workday2);
        arrayNode.add(weekend2);

        return arrayNode;
    }

}
