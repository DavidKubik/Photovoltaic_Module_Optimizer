package sk.stuba.fiit.kubikd.photovoltaic_module_optimizer_client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private float latitude;
    private float longitude;
    private float altitude;
    private int pvNum;
    private int batteryNum;
    private float azimuth;
    private boolean pitchedRoof = true;
    private float elPrice;
    private float pvPrice;
    private List<Float> workday;
    private boolean useWeekend = true;
    private List<Float> weekend;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout2);

        checkBox = (CheckBox) findViewById(R.id.weekendCheckB);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    gridLayout.setVisibility(View.VISIBLE);
                } else {
                    gridLayout.setVisibility(View.GONE);
                }
            }
        });

        final SeekBar modulesSeekBar = (SeekBar) findViewById(R.id.moduleSeekBar);
        modulesSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView moduleNumShowTxtV = findViewById(R.id.moduleNumShowTxtV);
                int min = 1;
                if(progress < min) {
                    seekBar.setProgress(min);
                    moduleNumShowTxtV.setText(String.valueOf(min));
                } else {
                    moduleNumShowTxtV.setText(String.valueOf(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        final SeekBar batterySeekBar = (SeekBar) findViewById(R.id.batterySeekBar);
        batterySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView batteryNumShowTxtV = findViewById(R.id.batteryNumShowTxtV);
                batteryNumShowTxtV.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.pitchedRadioB:
                if (checked)
                    pitchedRoof = true;
                    break;
            case R.id.facadeRadioB:
                if (checked)
                    pitchedRoof = false;
                    break;
        }
    }

    public void openCalcScreen(View view) {
        //latitude:
        EditText latitudeEditT = (EditText) findViewById(R.id.latitudeEditT);
        if (latitudeEditT.getText().toString().equals("") ||
                latitudeEditT.getText().toString().equals("-")){
            latitude = 0;
        } else {
            latitude = Float.parseFloat(String.valueOf(latitudeEditT.getText()));
            if (latitude < -90 || latitude > 90) {
                latitudeEditT.setError(getString(R.string.latitudeErr));
            }
        }
        Log.i("MainActivity", "latitude is set to: " + latitude);

        //longitude:
        EditText longitudeEditT = (EditText) findViewById(R.id.longitudeEditT);
        if (longitudeEditT.getText().toString().equals("") ||
                longitudeEditT.getText().toString().equals("-")) {
            longitude = 0;
        } else {
            longitude = Float.parseFloat(String.valueOf(longitudeEditT.getText()));
            if (longitude < -180 || longitude > 180) {
                longitudeEditT.setError(getString(R.string.longitudeErr));
            }
        }
        Log.i("MainActivity", "longitude is set to: " + longitude);

        //altitude:
        EditText altitudeEditT = (EditText) findViewById(R.id.altitudeEditT);
        if (altitudeEditT.getText().toString().equals("") ||
                altitudeEditT.getText().toString().equals("-")) {
            altitude = 0;
        } else {
            altitude = Float.parseFloat(String.valueOf(altitudeEditT.getText()));
        }
        Log.i("MainActivity", "altitude is set to: " + altitude);

        //pvNum:
        SeekBar moduleSeekBar = (SeekBar) findViewById(R.id.moduleSeekBar);
        pvNum = moduleSeekBar.getProgress();
        Log.i("MainActivity", "pvNum is set to: " + pvNum + " (SeekBar)");

        //batteryNum:
        SeekBar batterySeekBar = (SeekBar) findViewById(R.id.batterySeekBar);
        batteryNum = batterySeekBar.getProgress();
        Log.i("MainActivity", "batteryNum is set to: " + batteryNum + " (SeekBar)");

        //azimuth:
        EditText azimuthEditT = (EditText) findViewById(R.id.azimuthEditT);
        if (azimuthEditT.getText().toString().equals("")) {
            azimuth = 0;
        } else {
            azimuth = Float.parseFloat(String.valueOf(azimuthEditT.getText()));
            if (azimuth > 359.999999) {
                azimuthEditT.setError(getString(R.string.azimuthErr));
            }
        }
        Log.i("MainActivity", "azimuth is set to: " + azimuth);

        //pitchedRoof:
        Log.i("MainActivity", "pitchedRoof is set to: " + pitchedRoof);

        //elPrice:
        EditText priceElEditT = (EditText) findViewById(R.id.priceElEditT);
        if (priceElEditT.getText().toString().equals("")) {
            elPrice = 0;
        } else {
            elPrice = Float.parseFloat(String.valueOf(priceElEditT.getText()));
        }
        Log.i("MainActivity", "elPrice is set to: " + elPrice);

        //pvPrice:
        EditText pricePvEditT = (EditText) findViewById(R.id.pricePvEditT);
        if (pricePvEditT.getText().toString().equals("")) {
            pvPrice = 0;
        } else {
            pvPrice = Float.parseFloat(String.valueOf(pricePvEditT.getText()));
        }
        Log.i("MainActivity", "pvPrice is set to: " + pvPrice);

        //workday electricity consumption:
        workday = new ArrayList<>(24);
        EditText zeroWork = (EditText) findViewById(R.id.zeroEditT);
        EditText oneWork = (EditText) findViewById(R.id.oneEditT);
        EditText twoWork = (EditText) findViewById(R.id.twoEditT);
        EditText threeWork = (EditText) findViewById(R.id.threeEditT);
        EditText fourWork = (EditText) findViewById(R.id.fourEditT);
        EditText fiveWork = (EditText) findViewById(R.id.fiveEditT);
        EditText sixWork = (EditText) findViewById(R.id.sixEditT);
        EditText sevenWork = (EditText) findViewById(R.id.sevenEditT);
        EditText eightWork = (EditText) findViewById(R.id.eightEditT);
        EditText nineWork = (EditText) findViewById(R.id.nineEditT);
        EditText tenWork = (EditText) findViewById(R.id.tenEditT);
        EditText elevenWork = (EditText) findViewById(R.id.elevenEditT);
        EditText twelveWork = (EditText) findViewById(R.id.twelveEditT);
        EditText thirteenWork = (EditText) findViewById(R.id.thirteenEditT);
        EditText fourteenWork = (EditText) findViewById(R.id.fourteenEditT);
        EditText fifteenWork = (EditText) findViewById(R.id.fifteenEditT);
        EditText sixteenWork = (EditText) findViewById(R.id.sixteenEditT);
        EditText seventeenWork = (EditText) findViewById(R.id.seventeenEditT);
        EditText eighteenWork = (EditText) findViewById(R.id.eighteenEditT);
        EditText nineteenWork = (EditText) findViewById(R.id.nineteenEditT);
        EditText twentyWork = (EditText) findViewById(R.id.twentyEditT);
        EditText twentyoneWork = (EditText) findViewById(R.id.twentyoneEditT);
        EditText twentytwoWork = (EditText) findViewById(R.id.twentytwoEditT);
        EditText twentythreeWork = (EditText) findViewById(R.id.twentythreeEditT);
        if (zeroWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(zeroWork.getText())));
        }
        if (oneWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(oneWork.getText())));
        }
        if (twoWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(twoWork.getText())));
        }
        if (threeWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(threeWork.getText())));
        }
        if (fourWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(fourWork.getText())));
        }
        if (fiveWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(fiveWork.getText())));
        }
        if (sixWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(sixWork.getText())));
        }
        if (sevenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(sevenWork.getText())));
        }
        if (eightWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(eightWork.getText())));
        }
        if (nineWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(nineWork.getText())));
        }
        if (tenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(tenWork.getText())));
        }
        if (elevenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(elevenWork.getText())));
        }
        if (twelveWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(twelveWork.getText())));
        }
        if (thirteenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(thirteenWork.getText())));
        }
        if (fourteenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(fourteenWork.getText())));
        }
        if (fifteenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(fifteenWork.getText())));
        }
        if (sixteenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(sixteenWork.getText())));
        }
        if (seventeenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(seventeenWork.getText())));
        }
        if (eighteenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(eighteenWork.getText())));
        }
        if (nineteenWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(nineteenWork.getText())));
        }
        if (twentyWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(twentyWork.getText())));
        }
        if (twentyoneWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(twentyoneWork.getText())));
        }
        if (twentytwoWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(twentytwoWork.getText())));
        }
        if (twentythreeWork.getText().toString().equals("")) {
            workday.add(Float.parseFloat("0"));
        } else {
            workday.add(Float.parseFloat(String.valueOf(twentythreeWork.getText())));
        }

        //use weekend el. consumption data:
        CheckBox weekendCheckB = (CheckBox) findViewById(R.id.weekendCheckB);
        useWeekend = weekendCheckB.isChecked();
        Log.i("MainActivity", "useWeekend is set to: " + useWeekend);

        //weekend electricity consumption:
        weekend = new ArrayList<>(24);
        EditText zeroWeek = (EditText) findViewById(R.id.zeroEditT2);
        EditText oneWeek = (EditText) findViewById(R.id.oneEditT2);
        EditText twoWeek = (EditText) findViewById(R.id.twoEditT2);
        EditText threeWeek = (EditText) findViewById(R.id.threeEditT2);
        EditText fourWeek = (EditText) findViewById(R.id.fourEditT2);
        EditText fiveWeek = (EditText) findViewById(R.id.fiveEditT2);
        EditText sixWeek = (EditText) findViewById(R.id.sixEditT2);
        EditText sevenWeek = (EditText) findViewById(R.id.sevenEditT2);
        EditText eightWeek = (EditText) findViewById(R.id.eightEditT2);
        EditText nineWeek = (EditText) findViewById(R.id.nineEditT2);
        EditText tenWeek = (EditText) findViewById(R.id.tenEditT2);
        EditText elevenWeek = (EditText) findViewById(R.id.elevenEditT2);
        EditText twelveWeek = (EditText) findViewById(R.id.twelveEditT2);
        EditText thirteenWeek = (EditText) findViewById(R.id.thirteenEditT2);
        EditText fourteenWeek = (EditText) findViewById(R.id.fourteenEditT2);
        EditText fifteenWeek = (EditText) findViewById(R.id.fifteenEditT2);
        EditText sixteenWeek = (EditText) findViewById(R.id.sixteenEditT2);
        EditText seventeenWeek = (EditText) findViewById(R.id.seventeenEditT2);
        EditText eighteenWeek = (EditText) findViewById(R.id.eighteenEditT2);
        EditText nineteenWeek = (EditText) findViewById(R.id.nineteenEditT2);
        EditText twentyWeek = (EditText) findViewById(R.id.twentyEditT2);
        EditText twentyoneWeek = (EditText) findViewById(R.id.twentyoneEditT2);
        EditText twentytwoWeek = (EditText) findViewById(R.id.twentytwoEditT2);
        EditText twentythreeWeek = (EditText) findViewById(R.id.twentythreeEditT2);
        if (zeroWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(zeroWeek.getText())));
        }
        if (oneWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(oneWeek.getText())));
        }
        if (twoWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(twoWeek.getText())));
        }
        if (threeWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(threeWeek.getText())));
        }
        if (fourWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(fourWeek.getText())));
        }
        if (fiveWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(fiveWeek.getText())));
        }
        if (sixWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(sixWeek.getText())));
        }
        if (sevenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(sevenWeek.getText())));
        }
        if (eightWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(eightWeek.getText())));
        }
        if (nineWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(nineWeek.getText())));
        }
        if (tenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(tenWeek.getText())));
        }
        if (elevenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(elevenWeek.getText())));
        }
        if (twelveWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(twelveWeek.getText())));
        }
        if (thirteenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(thirteenWeek.getText())));
        }
        if (fourteenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(fourteenWeek.getText())));
        }
        if (fifteenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(fifteenWeek.getText())));
        }
        if (sixteenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(sixteenWeek.getText())));
        }
        if (seventeenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(seventeenWeek.getText())));
        }
        if (eighteenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(eighteenWeek.getText())));
        }
        if (nineteenWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(nineteenWeek.getText())));
        }
        if (twentyWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(twentyWeek.getText())));
        }
        if (twentyoneWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(twentyoneWeek.getText())));
        }
        if (twentytwoWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(twentytwoWeek.getText())));
        }
        if (twentythreeWeek.getText().toString().equals("")) {
            weekend.add(Float.parseFloat("0"));
        } else {
            weekend.add(Float.parseFloat(String.valueOf(twentythreeWeek.getText())));
        }

        if (azimuth <= 359.999999 && ((latitude >= - 90) && (latitude <= 90)) && ((longitude >= -180) &&
                (longitude <= 180))) {
            MyJson myJson = new MyJson(latitude, longitude, altitude, pvNum, batteryNum, azimuth,
                    pitchedRoof, elPrice, pvPrice, useWeekend,
                    workday, weekend);

            Intent myIntent = new Intent(MainActivity.this, ComputationActivity.class);
            myIntent.putExtra("params",  new Gson().toJson(myJson));
            MainActivity.this.startActivity(myIntent);
        }
    }

    public void openAboutScreen(View view) {
        Intent myIntent = new Intent(MainActivity.this, AboutActivity.class);
        MainActivity.this.startActivity(myIntent);
    }
}
