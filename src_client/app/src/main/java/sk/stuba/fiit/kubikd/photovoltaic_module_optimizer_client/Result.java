package sk.stuba.fiit.kubikd.photovoltaic_module_optimizer_client;

/**
 * Created by David on 4.4.2018.
 */

public class Result {
    private String pv_name;
    private String timezone;
    private float annual_generated;
    private float annual_consumption;
    private float cov_system;
    private float cov_pv;
    private float cov_battery;
    private float annual_savings;
    private float max_power;
    private float max_consumption;
    private int battery_rep_num;
    private float battery_costs;
    private float pv_costs;
    private float inverter_costs;
    private float controller_costs;
    private float pv_install;
    private float system_install;
    private float battery_install;
    private float controller_install;
    private float total_savings;
    private float total_costs;
    private float net_savings;
    private boolean pay_back;

    public void setPv_name(String pv_name) {
        this.pv_name = pv_name;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setAnnual_generated(float annual_generated) {
        this.annual_generated = annual_generated;
    }

    public void setAnnual_consumption(float annual_consumption) {
        this.annual_consumption = annual_consumption;
    }

    public void setCov_system(float cov_system) {
        this.cov_system = cov_system;
    }

    public void setCov_pv(float cov_pv) {
        this.cov_pv = cov_pv;
    }

    public void setCov_battery(float cov_battery) {
        this.cov_battery = cov_battery;
    }

    public void setAnnual_savings(float annual_savings) {
        this.annual_savings = annual_savings;
    }

    public void setMax_power(float max_power) {
        this.max_power = max_power;
    }

    public void setMax_consumption(float max_consumption) {
        this.max_consumption = max_consumption;
    }

    public void setBattery_rep_num(int battery_rep_num) {
        this.battery_rep_num = battery_rep_num;
    }

    public void setBattery_costs(float battery_costs) {
        this.battery_costs = battery_costs;
    }

    public void setPv_costs(float pv_costs) {
        this.pv_costs = pv_costs;
    }

    public void setInverter_costs(float inverter_costs) {
        this.inverter_costs = inverter_costs;
    }

    public void setController_costs(float controller_costs) {
        this.controller_costs = controller_costs;
    }

    public void setPv_install(float pv_install) {
        this.pv_install = pv_install;
    }

    public void setSystem_install(float system_install) {
        this.system_install = system_install;
    }

    public void setBattery_install(float battery_install) {
        this.battery_install = battery_install;
    }

    public void setController_install(float controller_install) {
        this.controller_install = controller_install;
    }

    public void setTotal_savings(float total_savings) {
        this.total_savings = total_savings;
    }

    public void setTotal_costs(float total_costs) {
        this.total_costs = total_costs;
    }

    public void setNet_savings(float net_savings) {
        this.net_savings = net_savings;
    }

    public void setPay_back(boolean pay_back) {
        this.pay_back = pay_back;
    }

    public String getPvName() {
        return pv_name;
    }

    public String getTimezone() {
        return timezone;
    }

    public float getAnnualGenerated() {
        return annual_generated;
    }

    public float getAnnualConsumption() {
        return annual_consumption;
    }

    public float getCovSystem() {
        return cov_system;
    }

    public float getCovPv() {
        return cov_pv;
    }

    public float getCovBattery() {
        return cov_battery;
    }

    public float getAnnualSavings() {
        return annual_savings;
    }

    public float getMaxPower() {
        return max_power;
    }

    public float getMaxConsumption() {
        return max_consumption;
    }

    public int getBatteryRepNum() {
        return battery_rep_num;
    }

    public float getBatteryCosts() {
        return battery_costs;
    }

    public float getPvCosts() {
        return pv_costs;
    }

    public float getInverterCosts() {
        return inverter_costs;
    }

    public float getControllerCosts() {
        return controller_costs;
    }

    public float getPvInstall() {
        return pv_install;
    }

    public float getSystemInstall() {
        return system_install;
    }

    public float getBatteryInstall() {
        return battery_install;
    }

    public float getControllerInstall() {
        return controller_install;
    }

    public float getTotalSavings() {
        return total_savings;
    }

    public float getTotalCosts() {
        return total_costs;
    }

    public float getNetSavings() {
        return net_savings;
    }

    public boolean isPayBack() {
        return pay_back;
    }
}
