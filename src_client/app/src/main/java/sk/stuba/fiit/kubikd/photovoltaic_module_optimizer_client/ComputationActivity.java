package sk.stuba.fiit.kubikd.photovoltaic_module_optimizer_client;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by David on 4.4.2018.
 */

public class ComputationActivity extends Activity {

    private class PostJsonTask extends AsyncTask<String, Void, Result> {
        @Override
        protected Result doInBackground(String... params) {
            try {
                final String url = params[0] + "/compute";

                String jsonParams = null;
                MyJson myJson = null;

                Bundle extras = getIntent().getExtras();

                if (extras != null) {
                    jsonParams = extras.getString("params");
                }

                if (jsonParams != null) {
                    myJson = new Gson().fromJson(jsonParams, MyJson.class);
                }

                Object jsonObject = myJson.getJson();
                Log.i("ComputationActivity", "JSON content: " + jsonObject);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Result result = restTemplate.postForObject(url, jsonObject, Result.class, "");

                return result;
            } catch (Exception e) {
                Log.e("ComputationActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Result result) {
            Intent myIntent = new Intent(ComputationActivity.this, OutputActivity.class);

            myIntent.putExtra("result",  new Gson().toJson(result));

            ComputationActivity.this.startActivity(myIntent);
            finish();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_computation);

        String SERVER_URL = "";

        SERVER_URL = ConfigHelper.getConfigValue(this, "serverUrl");
        Log.i("ComputationActivity", "Server URL is set to: " + SERVER_URL);

        new PostJsonTask().execute(SERVER_URL);
    }
}
